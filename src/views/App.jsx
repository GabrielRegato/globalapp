import React, { Component, Fragment } from 'react';
import cx from 'classnames';

// Utilities
import { validate, validateURL } from '../utilities/validates';

class App extends Component {
  constructor(props) {
    super(props);

    const formValues = {
      imageUrl: '',
      description: '',
    };

    const postsList = [];

    this.state = {
      formValues: formValues,
      validates: formValues,
      postsList: postsList,
    };
  }

  render() {
    const { validates } = this.state;
    const ObjectValues = Object.values(validates);
    const classActive = ObjectValues.every(function(i) { return i; });

    const classBtn = cx({
      'btn': true,
      'disabled' : !classActive,
      'primary': true,
      'cursor': classActive,
    });

    return (
      <Fragment>
        <div className='App'>
          <div className='App-form'>
            { this.renderPostForm() }
            <div>
              <button
                onClick = { this.handleAddPost }
                className={ classBtn } >
                Add New Post
              </button>
            </div>
          </div>
        </div>
        { this.renderPosts() }
      </Fragment>
    );
  }

  renderPostForm() {
    const { validates } = this.state;

    const fieldsArray = [
      {
        text: 'Image URL: ',
        type: 'text',
        key:  'imageUrl',
        errorMessage: 'Invalid URL format',
      },
      {
        text: 'Description: ',
        type: 'text',
        key:  'description',
        errorMessage: 'Description is required',
      },
    ];

    return (
      <form autoComplete='off'>
        { fieldsArray.map((obj, index)=> {
          const classInput = cx({
            'addformGroup-error-add' : !validates[obj.key] && validates[obj.key] !== '' && validates[obj.key] !== undefined,
          });

          return (
            <div className='addformGroup' key={index} >
              <label className='text-dark'>{ obj.text }</label>
              <input
                ref={obj.key}
                className={ classInput }
                type={ obj.type }
                onBlur={ evt => this.validateField(evt, obj.key, obj.lengthField) }
                onChange={ evt => this.updateInputValue(evt, obj.key) } />
              { !validates[obj.key] && validates[obj.key] !== '' && validates[obj.key] !== undefined &&
                <span className='formGroup-error'>{ obj.errorMessage }</span>
              }
            </div>
          );
        })}
      </form>
    );
  }

  renderPosts() {
    const { postsList } = this.state;
    const postClasses = cx({
      'App-form': true,
      'App-post': true,
    });
    return (
      <div className='App'>
        {
          postsList.map((post, index) => {
            return (
              <div className={ postClasses } key={index} >
                <div className='post-title'>
                  <h1>{post.description}</h1>
                </div>
                <img
                  src={post.imageUrl}
                  className='imageFormat'
                  title={post.description} />
              </div>
            );
          })
        }
      </div>
    );
  }

  handleAddPost = () => {
    const { formValues, postsList } = this.state;
    const posts = postsList;
    posts.push(formValues);

    this.cleanForm();

    const emptyValues = {
      imageUrl: '',
      description: '',
    };

    this.setState({
      postsList: posts,
      formValues: emptyValues,
      validates: emptyValues,
    });
  }

  cleanForm = () => {
    this.refs.imageUrl.value = '';
    this.refs.description.value = '';
  }

  updateInputValue(evt, key) {
    const inputValue = evt.target.value;

    this.setState(prevState => ({
      formValues: {
        ...prevState.formValues,
        [key]: inputValue,
      },
    }));
  }

  validateField(evt, key, lengthField) {
    const inputValue = evt.target.value;
    let validateOption = undefined;

    if (key === 'imageUrl') {
      validateOption = validateURL(inputValue);
    } else {
      validateOption = validate(inputValue, lengthField);
    }

    this.setState(prevState => ({
      validates : {
        ...prevState.validates,
        [key] : validateOption,
      },
    }));
  }
}

export default App;

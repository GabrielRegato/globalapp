import React from 'react';
import logo from '../img/logo.svg';

const NotFound = () => {
  return (
    <div className='NotFound'>
      <header className='NotFound-header'>
        <img
          alt='logo'
          className='NotFound-logo'
          src={logo} />
        <p>
          404 Not Found
        </p>
      </header>
    </div>
  );
};

export default NotFound;

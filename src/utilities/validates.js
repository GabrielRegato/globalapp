export const validate = (text, lengthField) => {
  if (text != '' && text !== null & text !== undefined && ( lengthField ? ( text.length === lengthField ) : true )) {
    return true;
  } else {
    return false;
  }
};

export const validateURL = imageUrl => {
  if (/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/.test(imageUrl) && imageUrl.length > 0 ) {
    return true;
  } else {
    return false;
  }
};
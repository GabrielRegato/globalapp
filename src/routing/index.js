import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import App from '../views/App';
import NotFound from '../views/NotFound';
const Router = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact={true} path={ '/' }
          component= { App } />
        <Route component= { NotFound } />
      </Switch>
    </BrowserRouter>
  );
};

export default Router;